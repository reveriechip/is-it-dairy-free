let fetch;

exports.handler = async (event) => {
    if (!fetch) {
        fetch = (await import('node-fetch')).default;
    }

    const API_SECRET = process.env.API_SECRET;
    if (!API_SECRET) {
        return {
            statusCode: 500,
            body: 'API_SECRET environment variable is not set'
        };
    }

    const query = event.queryStringParameters.query;
    if (!query) {
        return {
            statusCode: 400,
            body: 'query parameter is required'
        };
    }

    try {
        const response = await fetch(`https://nutrition-api.esha.com/foods?query=${query}&start=0&count=100`, {
            method: 'GET',
            headers: {
                'Accept': 'application/json',
                'Ocp-Apim-Subscription-Key': API_SECRET
            }
        });

        const data = await response.json();

        return {
            statusCode: 200,
            body: JSON.stringify(data)
        };
    } catch (error) {
        return {
            statusCode: 500,
            body: `Fetch request failed: ${error.message}`
        };
    }
};
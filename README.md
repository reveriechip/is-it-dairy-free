# IsItDairyFree

## Description
IsItDairyFree is a web application that helps users determine if a food item is dairy-free or not. It provides a simple interface where users can search for a food item and get information about its dairy content.

## Features
- Search for food items
- Display dairy-free status of food items
- User-friendly interface

## Technologies Used
- HTML
- CSS
- JavaScript
- ESHA Nutrition API for retrieving food data

## Usage
1. Enter the name of a food item in the search bar.
2. Click the "Search" button.
3. The dairy-free status of the food item will be displayed.
document.addEventListener("DOMContentLoaded", function () {
    const searchBox = document.getElementById('searchBox');
    const searchBtn = document.getElementById('searchBtn');
    const resultsDiv = document.getElementById('results');

    searchBtn.addEventListener("click", function () {
        // console.log('Search button clicked');
        const searchParam = searchBox.value;
        if (searchParam === "") {
            console.log('Search parameter is empty');
            resultsDiv.innerHTML = "";
            return;
        }
        console.log('Making fetch request with query:', searchParam);
        fetch(`/.netlify/functions/search?query=${searchParam}`)
            .then(response => {
                // console.log('Received response:', response);
                if (!response.ok) {
                    return response.text().then(text => { throw new Error(text) });
                }
                return response.json();
            })
            .then(data => {
                // console.log('Received data:', data);
                LogData(data);
            })
            .catch(error => {
                console.error('Error:', error);
                resultsDiv.innerHTML = `<p class="error">Error: ${error.message}</p>`;
            });
    });

    function LogData(data) {
        // console.log('Processing data:', data);
        if (!data || !data.items || data.items.length === 0) {
            // console.log('No data received or data is empty');
            resultsDiv.innerHTML = "<p>No results found.</p>";
            return;
        }

        resultsDiv.innerHTML = "";
        let htmlString = "";
        let colorType = "";

        data.items.forEach(item => {
            if ('characteristics' in item) {
                if (item.characteristics.includes("Dairy Free")) {
                    colorType = "greenListItem";
                    htmlString += `<div class='listItem module ${colorType}'><p>${item.product} - ${item.description}<br />DAIRY FREE <i class='checkmark fas fa-check-square'></i></p></div>`;
                } else {
                    colorType = "redListItem";
                    htmlString += `<div class='listItem module ${colorType}'><p>${item.product} - ${item.description}<br />CONTAINS DAIRY <i class='cancel fas fa-times-circle'></i> </p></div>`;
                }
            } else {
                // console.log('Item does not have characteristics:', item);
            }
        });

        if (htmlString !== "") {
            resultsDiv.insertAdjacentHTML('beforeend', htmlString);
        } else {
            resultsDiv.innerHTML = "<p>No results match your search criteria.</p>";
        }
    }
});
